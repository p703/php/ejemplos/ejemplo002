<!DOCTYPE html>
<?php
$radio=2.4;
$perimetro=2*M_PI*$radio; //calcular el perimetro del circulo;
$area=pi()*$radio**2; //calcular el area de un circulo;
?>
<div>
    El radio del circulo es <?= $radio ?>
</div>
<div>
    El area del circulo es <?= $area ?>
</div>
<div>
    El perimetro del circulo es <?= $perimetro ?>
</div>
