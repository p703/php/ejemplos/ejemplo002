<?php
if (!empty($_REQUEST)){//pregunta si esta vacio 
    if ($_REQUEST["numero"] > 0){
        $caso = "bien";//muestra el var_dump al dar al boton ENVIAR
    }else{
        $caso = "mal";//la primera vez que se carga el formulario
    }
}else{
    $caso = "mal";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <!<!-- Los estilos hacen que quede más bonito el formulario -->
        <style type="text/cas">
            input[type="number"]{
                width: 300px;
            }
            .obligatorio::before{
                content:"Obligatorio";
                min-width: 150px;
                display: inline-block;
            }
            .noObligatorio::before{
                content:"opcional";
                min-width: 150px;
                display:inline-block;
            }
        </style>
    </head>
    <body>
        <?php
        //esto es para cuando pulsan boton
        if ($caso == "bien"){
            var_dump($_REQUEST);
        //esto es para cada vez que se carga por primera vez la pagina
        }else{
        ?>
        <div>
            <form name="f">
                <div class="obligatorio"><input required placeholder="Introduce un numero" step="1" min="1" max="100" type="number" name="numero" /></div>
                <div class="noObligatorio"><input placeholder="Introduce un numero" step="1" min="1" max="100" type="number" name="numero1" /></div>
                <input type="submit" value="Enviar" name="boton" />
            </form>
        </div>
        <?php
        }
        ?>
    </body>
</html>