<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        class Coche{
            
            public $color;
            public $numero_puertas;
            public $marca;
            public $gasolina = 0;
            
            //metodo publico para echar gasolina al coche
            public function llenarTanque($gasolina_nueva){
                $this->gasolina = $this->gasolina + $gasolina_nueva;
            }
            
            //metodo publico para acelerar el coche
            //este metodo comprueba si te queda gasolina 
            //si hay gasolina le resta un litro y te devuelve cuanta gasolina te queda
            function acelerar(){
                if ($this->gasolina > 0){
                    $this->gasolina = $this->gasolina -1;
                    return 'Gasolina restante: ' . $this->gasolina;
                }
            }
        }
        ?>
    </body>
</html>
