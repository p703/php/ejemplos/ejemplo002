<?php
    require './ejercicio5/Vehiculo.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //instancio un objeto e tipo Vehiculo
        //ademas inicializo el vehiculo
        $ford=new Vehiculo("DHH2323","rojo",false);
        var_dump($ford);
        
        $ford->apagar();
        
        //instancio otro objeto de tipo vehiculo
        $renault=new Vehiculo();
        var_dump($renault);
        ?>
    </body>
</html>
