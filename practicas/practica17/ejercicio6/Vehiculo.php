<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        class Vehiculo{
            
            public $matricula;
            private $color;
            protected $encendido;
            public static $ruedas=5;
            
            function __construct($matricula,$color,$encendido){
                $this->matricula = $matricula;
                $this->color = $color;
                $this->encendido = $encendido;
            }
            
            public static function encender(){
                $this->encendido = true;
                echo 'Vehiculo encendido <br />';
            }
            
            static function mensaje(){
                echo "Este es mi coche";
            }
            
            static function ruedas(){
                echo Vehiculo::$ruedas;
            }
        }
        ?>
    </body>
</html>
