<?php
    include "ejercicio4/Coche.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        $coche = new Coche();//Instanciamos la clase carro
        $coche->color = 'Rojo';//Llenamos algunas de las propiedades
        $coche->marca = 'Honda';
        $coche->numero_puertas = 4;
        $coche->llenarTanque(10);//echamos gasolina al coche (10 litros)
        
        echo $coche->acelerar();//aceleramos el coche y mostramos la gasolina que queda
        $coche->acelerar();//acelero el coche 
        echo $coche->acelerar();
        
        var_dump($coche);
        
        ?>
    </body>
</html>
