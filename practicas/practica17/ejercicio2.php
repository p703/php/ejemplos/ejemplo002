<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        class Usuario {
            
            public $nombre = "defecto";
            private $edad;//propiedad privada solo accesible por la clase
            protected $telefono;//propiedad protegida a la que se puede acceder desde la clase y desde los hijos
            
            //metodos
            
            //getters
            //metodos para leer las propiedades
            public function getNombre(){
                return $this->nombre;
            }
            
            public function getEdad(){
                return $this->edad;
            }
            
            public function getTelefono(){
                return $this->telefono;
            }
            
            //setters
            //metodos para escribir en las propiedades
            public function setNombre($nombre){
                $this->nombre = $nombre;
            }
            
            public function setEdad($edad){
                $this->edad = $edad;
            }
            
            public function setTelefono($telefono){
                $this->telefono = $telefono;
            }
        }
        
        
        
        /**
         * Vamos a crear objetos de tipo usuario
         */
        $persona = new Usuario();
        
        //leer el nombre de la persona (defecto)
        echo $persona->nombre;
        
        //escribo en la propiedad edad 51
        $persona->setEdad(51);
        
        //escribo en la propiedad telefono
        $persona->setTelefono("232323");
        //vuelco en pantalla los valores del objeto
        var_dump($persona);
        
        //escribiendo el nombre de la persona sin utilizar setter
        $persona->nombre = "Silvia";
        
        //escribiendo la edad de la persona sin utilizar el setter
        //$persona->edad=12; //nos produce error ya que la propiedad es privada
        
        //para acceder a la propiedad edad tengo que utilizar el setter
        $persona->setEdad(12);
        
        //escribiendo el telefono sin utilizar el setter
        //$persona->telefono="202020"; //nos produce error ya que telefono es protected
        
        //utilizamos el setter
        $persona->setTelefono("202020");
        
        var_dump($persona);
        
        ?>
    </body>
</html>
