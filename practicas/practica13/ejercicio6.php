<!DOCTYPE html>
<?php
function ejercicio6($numeros){
    /**
     * Bucle para mostrar array de numeros
     */
    //Mediante foreach(mas recomendable para moverse por arrays:
    foreach($numeros as $valor){
        echo "<br/>{$valor}<br/>";
    }
    //Mediante for:
    for($c=0;$c<count($numeros);$c++){
        echo "<br/>{$numeros[$c]}<br/>";
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        ejercicio6([4,5,8,1]);
        ?>
    </body>
</html>
