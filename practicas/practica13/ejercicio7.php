<!DOCTYPE html>
<?php
/**
 * Solucion con if y else
 */
function ejercicio7($numero1,$numero2){
    $salida=null;
    if($numero1==$numero2){
        $salida="Iguales";//echo "<br/>Iguales<br/>";
    }else{
        $salida="Distintos";//echo "<br/>Distintos<br/>";
    }
    echo $salida;
}
/**
 * Solucion solo con if
 */
function ejercicio7a($numero1,$numero2){
    $salida="Distintos";
    
    if($numero1==$numero2){
        $salida="Iguales";
    }
    echo $salida;
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        ejercicio7(2,4);
        ejercicio7(2,2);
        ejercicio7a(2,4);
        ejercicio7a(2,2);
        ?>
    </body>
</html>
