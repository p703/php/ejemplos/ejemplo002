<!DOCTYPE html>
<?php
/**
 * Version en la que utilizo la funcion de php
 * @param type $numeros
 * @return type
 */
function ejercicio14($numeros){
    return array_sum($numeros);
}

/**
 * Version en la que recorro el array para ir sumando los elementos
 * @param type $numeros
 * @return type
 */
function ejercicio14v1($numeros){
    $resultado=0;
    foreach ($numeros as $numero) {
        $resultado=$resultado+$numero;
    }
    return $resultado;
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo ejercicio14([1,2,3,4,5]);
        echo ejercicio14v1([1,2,3,4,5]);
        ?>
    </body>
</html>
