<!DOCTYPE html>
<?php
//If y else:
function ejercicio8($numero1,$numero2){
    $salida=null;
    if($numero1==$numero2){
        $salida="Iguales";//echo "<br/>Iguales<br/>";
    }else{
        $salida="Distintos";//echo "<br/>Distintos<br/>";
    }
    return $salida;
}

//Solo if:
    function ejercicio8a($numero1,$numero2){
    $salida="Distintos";
    
    if($numero1==$numero2){
        $salida="Iguales";
    }
    return $salida;
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo ejercicio8(2,4);
        echo ejercicio8(2,2);
        echo ejercicio8a(2,4);
        echo ejercicio8a(2,2);
        ?>
    </body>
</html>
