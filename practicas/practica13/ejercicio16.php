<?php
function ejercicio16($numero1,$numero2){
    global $resultado;
    $resultado=$numero1+$numero2;
}

function ejercicio16singlobal($numero1,$numero2){
    
    return $numero1+$numero2;
}
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $resultado=0;
        ejercicio16(2,3);
        echo $resultado;
        $resultado=ejercicio16singlobal(2,12);
        echo $resultado;
        ?>
    </body>
</html>
