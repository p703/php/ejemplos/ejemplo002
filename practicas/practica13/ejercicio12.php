<!DOCTYPE html>
<?php
/**
 * Version utilizando la funcion de php
 * strrev
 * @param type $texto
 */
function ejercicio12($texto){
    $resultado=strrev($texto);//me coloca el texto al reves
        echo "<br/>{$resultado}<br/>";
}

/**
 * Version en la que convierte el string en un array con la funcion
 * str_split
 * @param type $texto
 */
function ejercicio12v1($texto){
    $vector= str_split($texto);//me crea un array con el string
    for($c=count($vector)-1;$c>=0;$c--){
        echo "<br/>{$vector[$c]}<br/>";
    }  
}

/**
 * Version que muestro el texto girado tratando el texto como un array
 * @param type $texto
 */
function ejercicio12v2($texto){
    for($c= strlen($texto)-1;$c>=0;$c--){
        echo "<br/>{$texto[$c]}<br/>";//voy a tratar el texto como si fuera un array
    }
}

/**
 * Version en la que utilizo mi propia funcion strrev
 * @param type $texto
 * @return string
 */
function mistrrev($texto){
    $resultado="";
    for($c= strlen($texto)-1;$c>=0;$c--){
        $resultado=$resultado . $texto[$c]; 
    }
    return $resultado;
}

/**
 * Version en la que utilizo mi propia funcion strrev
 * @param type $texto
 */
function ejercicio12v3($texto){
    $resultado=mistrrev($texto);//me coloca el texto al reves
        echo "<br/>{$resultado}<br/>";
}

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        ejercicio12("Un texto a girar");
        ejercicio12v1("Un texto a girar");
        ejercicio12v2("Un texto a girar");
        ejercicio12v3("Un texto a girar");
        ?>
    </body>
</html>

