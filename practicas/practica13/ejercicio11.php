<!DOCTYPE html>
<?php
/**
 * Version inicial
 * leer directamente del final al principio
 * @param type $datos
 
function ejercicio11($datos){
    $indiceUltimoElemento=count($datos)-1;
    for($c=$indiceUltimoElemento;$c>=0;$c--){
       echo $datos[$c]; 
    }
}
*/


/**
 * Version utilizando la funcion de php
 * que nos da la vuelta a una array
 * @param type $datos
 */
function ejercicio11($datos){
    $datosAlReves= array_reverse($datos);
    foreach($datosAlReves as $valor){
        echo $valor;
    }
}

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        ejercicio11([1,"hola",23,"adios"]);
        ?>
    </body>
</html>
