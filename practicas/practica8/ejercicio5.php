<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="ejercicio5Destino.php" method="get">
            <div>
                <label for="nombre">
                    Nombre:
                </label>
                <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre">
            </div>
            <br>
            <div>
                <label for="apellidos">
                    Apellidos:
                </label>
                <input type="text" id="apellidos" name="apellidos" placeholder="Introduce tus apellidos">
            </div>
            <br>
            <div>
                <label for="direccion">
                    Dirección:
                </label>
                <input type="text" id="direccion" name="direccion" placeholder="Introduce tu dirección">
            </div>
            <br>
            <div>
                <label for="codigopostal">
                    Código postal:
                </label>
                <input type="text" id="codigopostal" name="codigopostal" placeholder="Introduce tu código postal">
            </div>
            <br>
            <div>
                <label for="telefono">
                    Teléfono:
                </label>
                <input type="text" id="telefono" name="telefono" placeholder="Introduce tu teléfono">
            </div>
            <br>
            <div>
                <label for="correo">
                    Correo:
                </label>
                <input type="text" id="correo" name="correo" placeholder="Introduce tu correo">
            </div>
            <br>
            <div>
            <button>ENVIAR</button>
            </div>
    </body>
</html>
<!--Name e ID simpre sin espacios para que no de error