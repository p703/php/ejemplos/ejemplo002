<?php

$a=190;
$b=["poco","algo","medio","mucho","enorme"];

if($a<10){
    echo $b[0];
}elseif($a<20){ 
    echo $b[1];
}elseif ($a<30){
    echo $b[2];
}elseif ($a<40){
    echo $b[3];
}else{
    echo $b[4];
}

// me devuelve "enorme" porque $a es mayor que los datos colocados en el elseif

// para que devuelva "poco" $a deberia valer 6 por ejemplo
// para que devuelva "algo" $a deberia valer 11 por ejemplo
// para que devuelva "medio" $a deberia valer 23 por ejemplo
// para que devuelva "mucho" $a deberia valer 37 por ejemplo
