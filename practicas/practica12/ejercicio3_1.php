<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $tiradas=[];
        //$sumaTiradas=[]; para array enumerado
        
        for($c=0;$c<10;$c++){
            //solucion con array enumerado
            //$tiradas[$c][0]=mt_rand(1,6);
            //$tiradas[$c][1]=mt_rand(1,6);
            //$sumaTiradas[$c]=$tiradas[$c][0]+$tiradas[$c][1];
            
            /*$tiradas[$c]["dado1"]=mt_rand(1,6);
            $tiradas[$c]["dado2"]=mt_rand(1,6);*/
            
            $tiradas[$c]=[
                "dado1" => mt_rand(1,6),
                "dado2" => mt_rand(1,6)
            ];
            $tiradas[$c]["suma"]=$tiradas[$c]["dado1"]+$tiradas[$c]["dado2"];
            
        }
        /*
        //array enumerado
        $dato=["ramon",48,185];
        echo $dato[0];
        //array asociativo inicializando todos a la vez
        $dato=[
            "nombre" => "ramon",
            "edad" => 48,
            "altura" => 185
        ];
        //array asociativo inicializando 1 a 1
        $dato["nombre"]="ramon";
        $dato["edad"]=48;
        $dato["altura"]=185;
        echo $dato["nombre"];
        */
        
        foreach($tiradas as $v){
        ?>
        <div>
            <div class="dados">
                <img src="imgs/<?= $v["dado1"] ?>.svg" alt="dado1"/>
                <img src="imgs/<?= $v["dado2"] ?>.svg" alt="dado2"/>
            </div>
            <div class="total">Total: <span> <?= $v["suma"] ?></span></div>
        </div>
        <?php
        
        }
        ?>
    </body>
    </html>
