<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $entero=10;
        $cadena="hola";
        $real=23.6;
        $logico=TRUE;
        
        var_dump($entero); //me muestra el valor de entero: 10
        var_dump($cadena); //me muestra el valor de cadena: 'hola'
        var_dump($real); //me muestra el valor de real: 23.6
        var_dump($logico); //me muestra el valor de logico: true
        
        $logico=(int)$logico;
        $entero=(float)$entero;
        settype($logico, "int");
        
        var_dump($entero); //me muestra el valor de entero: 10
        var_dump($cadena); //me muestra el valor de cadena: 'hola'
        var_dump($real); //me muestra el valor de real: 23.6
        var_dump($logico); //me muestra el numero entero que representa TRUE: 1 (FALSE= 0)
        
        ?>
    </body>
</html>
