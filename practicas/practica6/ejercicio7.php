<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $alumno1 = "Ramón";
        $alumno2 = "Jose";
        $alumno3 = "Pepe";
        $alumno4 = "Ana";
        
        //esto me muestra los valores de $alumno1 y $alumno2 en una linea 
        echo $alumno1;
        echo $alumno2;
        //esto me muestra el valor de $alumno3 en otra linea
        echo "<br>";
        echo "$alumno3";
        //esto me muestra el valor de $alumno4 en otra linea
        echo "<br>";
        echo $alumno4;
        ?>
        <div>
            <?php
            //esto me muestra todos los valores de las 4 variables en diferentes lineas gracias al <br>
            echo "$alumno1<br>$alumno2<br>$alumno3<br>$alumno4";
            ?>
        </div>
    </body>
</html>