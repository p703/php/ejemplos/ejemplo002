<?php

/**
 * Description of Ciudad
 *
 * @author Alpe
 */
class Ciudad {
    //propiedades
    public $nombre;
    private $provincia;
    
    //metodos
    public function getNombre() {
        return $this->nombre;
    }

    public function getProvincia() {
        return $this->provincia;
    }

    public function setNombre($nombre): void {
        $this->nombre = $nombre;
    }

    public function setProvincia($provincia): void {
        $this->provincia = $provincia;
    }
    
    public function obtenerIniciales($numero){
        $resultado= substr($this->nombre,0,$numero) . " " . substr($this->provincia,0,$numero);
        return $resultado;
    }
}