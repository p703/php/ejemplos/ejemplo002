<?php

// crear un array vacio
$a=[]; // creacion de arrays actual
$b=array (); // creacion de arrays obsoleta

// crear un array con elementos
$c=[2,56,7,"ejemplo"]; // array enumerado

$d=[
    "nombre" => "Jose",
    "edad" => 23,
    "poblacion" => "Isla"
    ]; // array asociativo

           
// leer una posicion del array
# del array c quiero leer el texto
echo $c[3];

# del array d leer la poblacion
echo $d["poblacion"];


// modificar una posicion del array

# en el array c quiero cambiar ejemplo por ejemplos
$c[3]="ejemplos";


# modificar la edad a 50 en el array d
$d["edad"]=50;

// añaden elementos al array

# colocar elemento nuevo al final del array c (push)
$c[]=100;
var_dump($c);

# colocar elemento nuevo al final del array c (push)
array_push($c, 350);
var_dump($c);


# colocar un elemento nuevo en la posicion 50 del array c
$c[50]=1000;
var_dump($c);
echo count($c);


# colocar un elemnto nuevo en el array d
$d["peso"]=85;
var_dump ($d);


// crear un array de 2 dimensiones
$alumnos=[
    [
        "nombre" => "Ivan",
        "edad" => 25
    ],
    [
        "nombre" => "Eva",
        "edad" => 18,
    ],
];
var_dump($alumnos);

echo $alumnos[1]["edad"]; //me muestra la edad de eva

// otro array bidimensional
$numeros=[
    "pares" => [2,4,8,10],
    "impares" => [3,5],
];

var_dump($numeros);

echo $numeros["pares"][2]; // me muestra el numero 8





























