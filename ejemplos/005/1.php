<!DOCTYPE html>
<?php
/**
 * Funcion que imprime una lista con los 3 textos pasados
 * @param string $texto1
 * @param string $texto2
 * @param string $texto3
 */
function crear($texto1,$texto2,$texto3){
        echo "<ul>";
        echo "<li>{$texto1}</li>";
        echo "<li>{$texto2}</li>";
        echo "<li>{$texto3}</li>"; 
        echo "</ul>";
}
/**
 * Funcion que imprime una lista con los 3 textos pasados
 * @param string $texto1
 * @param string $texto2
 * @param string $texto3
 */
function crear1($texto1,$texto2,$texto3){
    ?>

        <ul>
        <li><?= $texto1 ?></li>
        <li><?= $texto2 ?></li>
        <li><?= $texto3 ?></li> 
        </ul>
<?php
}
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        crear("Lorena","Luis","Carmen");
        crear1("lunes","martes","miercoles");
        ?>
        
    </body>
</html>
