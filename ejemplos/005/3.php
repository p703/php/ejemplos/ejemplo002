<!DOCTYPE html>
<?php
/**
 * Funcion que retorna la suma y producto de dos numeros
 * @param float $numero1
 * @param float $numero2
 * @return float producto de los dos numeros
 */
    function operacion($numero1,$numero2,$operacion){
        $resultado=0;
        switch ($operacion) {
            case 'suma':
                $resultado=$numero1+$numero2;
                break;
            case 'producto':
                $resultado=$numero1*$numero2;
                break;
            default:
                $resultado="operacion no implementada";
        }
        return $resultado;
    }
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        echo operacion(2, 5, "suma");
        echo "<br>";
        echo operacion(5, 4, "producto") . "<br>";
        echo operacion(33, 4, "resta") . "<br>";
        ?>
    </body>
</html>

