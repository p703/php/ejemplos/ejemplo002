<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /**
         * Sale una foto aleatoria cada vez que cargas la pagina
         */
            $fotos=[
                'bici','bobina','caballito','cascos','comida','edificio','lunes'
            ];
            
            $numeroFotos=count($fotos);
            $indice= mt_rand(0,$numeroFotos-1);
            $foto=$fotos[$indice];
        ?>
        
        <img src="imgs/<?= $foto ?>.jpg" alt="una foto"/>
    </body>
</html>
